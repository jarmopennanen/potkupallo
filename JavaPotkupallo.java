/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javapotkupallo;

import java.util.*;


/**
 *
 * @author jarmo
 */
public class JavaPotkupallo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    // Pelaaja-olion tiedot talteen näihin arraytaulukoihin.    
    ArrayList<String> taitotaulu= new ArrayList<>();   
    ArrayList<String> roolitaulu= new ArrayList<>();   
    ArrayList<String> numerotaulu= new ArrayList<>(); 
    ArrayList<String> pallotaulu= new ArrayList<>();  
    
    
      
    int loppu = 1;
    int ind = 0;    
    Scanner nappis = new Scanner(System.in);
    
    
    System.out.println( "Aloitetaan potkupallopeli.");
    System.out.println( "(Lopetus = 'loppu').");
    System.out.println( "Pelissä 7 pelaajaa. Pallo on pelaajalla 1.");
    
    // Luodaan Pelaaja-oliot.
       String npallo = "1";
    String nnumero = "1";
    String nrooli = "maalivahti";
    // Rooli kohtainen taitoprosentti. 1-9.  
    String ntaito = "4";
                 
    Pelaaja pelaaja = new Pelaaja(ntaito,nnumero,nrooli,npallo);
    Pelaaja maalivahti = new Maalivahti(ntaito,nnumero,nrooli,npallo);
    taitotaulu.add(maalivahti.getTaidot());
    roolitaulu.add(maalivahti.getRooli());
    numerotaulu.add(maalivahti.getNumero());
    pallotaulu.add(maalivahti.getPallo());   
    npallo = "0";
    
    nnumero = "2";
    nrooli = "puolustaja";
    Pelaaja puolustaja1 = new Puolustaja(ntaito,nnumero,nrooli,npallo);
    taitotaulu.add(puolustaja1.getTaidot());
    roolitaulu.add(puolustaja1.getRooli());
    numerotaulu.add(puolustaja1.getNumero());
    pallotaulu.add(puolustaja1.getPallo());   
    
    nnumero = "3";
    nrooli = "puolustaja";
    Pelaaja puolustaja2 = new Puolustaja(ntaito,nnumero,nrooli,npallo);
    taitotaulu.add(puolustaja2.getTaidot());
    roolitaulu.add(puolustaja2.getRooli());
    numerotaulu.add(puolustaja2.getNumero());
    pallotaulu.add(puolustaja2.getPallo());  
    npallo = "0";
        
    nnumero = "4";
    nrooli = "keskikentta";
    Pelaaja keskikentta1 = new Keskikentta(ntaito,nnumero,nrooli,npallo);
    taitotaulu.add(keskikentta1.getTaidot());
    roolitaulu.add(keskikentta1.getRooli());
    numerotaulu.add(keskikentta1.getNumero());
    pallotaulu.add(keskikentta1.getPallo());   
    
    nnumero = "5";
    nrooli = "keskikentta";
    Pelaaja keskikentta2 = new Keskikentta(ntaito,nnumero,nrooli,npallo);
    taitotaulu.add(keskikentta2.getTaidot());
    roolitaulu.add(keskikentta2.getRooli());
    numerotaulu.add(keskikentta2.getNumero());
    pallotaulu.add(keskikentta2.getPallo());   
    
    nnumero = "6";
    nrooli = "hyokkaaja";
    Pelaaja hyokkaaja1 = new Hyokkaaja(ntaito,nnumero,nrooli,npallo);
    taitotaulu.add(hyokkaaja1.getTaidot());
    roolitaulu.add(hyokkaaja1.getRooli());
    numerotaulu.add(hyokkaaja1.getNumero());
    pallotaulu.add(hyokkaaja1.getPallo());   
    
    nnumero = "7";
    nrooli = "hyokkaaja";
    Pelaaja hyokkaaja2 = new Hyokkaaja(ntaito,nnumero,nrooli,npallo);
    taitotaulu.add(hyokkaaja2.getTaidot());
    roolitaulu.add(hyokkaaja2.getRooli());
    numerotaulu.add(hyokkaaja2.getNumero());
    pallotaulu.add(hyokkaaja2.getPallo());   
    
   
// Syötellään palloa pelaajalta toiselle. 
// pallo-muuttaja on pelaajan numero, jolla pallo on jalassa.
    String pallo = "1";
    loppu = 1;
    String syottaja = null;
    String syotettava = null;
    while ( 0 < loppu ) {
        
        System.out.print( "Anna pelaajan numero jolle syötetään: ("+pallo+"->) :");
        syotettava = nappis.nextLine();
        if ("loppu".equals(syotettava)) {
            loppu = 0;
            break;
            }
    
        
        int lkm = numerotaulu.size();
       
        for (int ix = 0; ix < lkm; ix++) {
            
            if (numerotaulu.get(ix).equals(pallo) && !"0".equals(pallotaulu.get(ix))){
                ntaito = taitotaulu.get(ix);
                nnumero = numerotaulu.get(ix);
                nrooli = roolitaulu.get(ix);
                npallo = pallotaulu.get(ix);
                if ("hyokkaaja".equals(nrooli)) {
                    Pelaaja pelaajax = new Hyokkaaja(ntaito,nnumero,nrooli,npallo); 
                    String okpallo = pelaajax.tuuriPallo(npallo);
                    if ("1".equals(okpallo)) {
                        System.out.print("* "+pelaajax.getRooli()+" syöttää pallon - ");
                        npallo = "0";
                        pelaajax.setPallo(npallo);
                        pallotaulu.set(ix,pelaajax.syotaPallo(npallo));
                    }
                    if ("maali".equals(okpallo) && "1".equals(syotettava)) {
                        pallo = "1";
                        syotettava = null;
                        pallotaulu.set(0,"1");
                        pallotaulu.set(ix,"0");
                        continue;
                        }
                    if (syotettava != "1" && "maali".equals(okpallo)) {
                         syotettava = null;
                         continue;
                        }
                    
                    if ("0".equals(okpallo)) {
                        System.out.print("* "+pallo+ " - null - ");
                        syotettava = null;
                        continue;
                                               
                    }
                                       
                        
                    }
            else {
                    Pelaaja pelaajax = new Pelaaja(ntaito,nnumero,nrooli,npallo);
                    pelaajax.getNumero();
                    pelaajax.getRooli();
                    System.out.print("* "+pelaajax.getRooli()+" syöttää pallon - ");
                    pallotaulu.set(ix,pelaajax.syotaPallo(npallo));
                }
                
                
                ix = lkm;
                
                }
        }
    
       
    
        if (syotettava == null) {
            lkm = -1;
                    }
        else
        {
            lkm = numerotaulu.size();
        }
        for (int ix = 0; ix < lkm; ix++) {
            if (numerotaulu.get(ix).equals(syotettava) && !"1".equals(pallotaulu.get(ix))){
                ntaito = taitotaulu.get(ix);
                nnumero = numerotaulu.get(ix);
                nrooli = roolitaulu.get(ix);
                npallo = pallotaulu.get(ix);
                if ("maalivahti".equals(nrooli)) {
                    Pelaaja pelaajax = new Maalivahti(ntaito,nnumero,nrooli,npallo); 
                    pallo = pelaajax.getNumero();
                    //pelaajax.getRooli();
                    String okpallo = pelaajax.tuuriPallo(npallo);
                    //System.out.println("tuuriPallo ..."+okpallo);
                    if ("1".equals(okpallo)) {
                        System.out.println(pelaajax.getRooli()+" nappaa pallon...");
                        pallotaulu.set(ix,pelaajax.nappaaPallo(npallo));
                    } 
                    else 
                        System.out.println(pelaajax.getRooli()+" torjunta epäonnistuu. Maali!");
                        pallotaulu.set(ix,"maali");
                    }
                    
                else 
                    {
                    Pelaaja pelaajax = new Pelaaja(ntaito,nnumero,nrooli,npallo);
                    pallo = pelaajax.getNumero();
                    pelaajax.getRooli();
                    System.out.println(pelaajax.getRooli()+" nappaa pallon.");
                    pallotaulu.set(ix,pelaajax.nappaaPallo(npallo));
                                    
                ix = lkm; 
                
            }
        }
        
    }
    }
    // Lopputilanne pelin jälkeen.
    
    System.out.println(" ");
    System.out.println("Taidot   " +taitotaulu);
    System.out.println("Numerot  "  +numerotaulu);
    System.out.println("Roolit  "   +roolitaulu);
    System.out.println("Pallo hallussa = 1  "   +pallotaulu);
    System.out.println("Pallo on pelaajalla "   +pallo);
    System.out.println("Loppu.   ");
    /**
     * 
    while ( 0 < loppu ) {
        //System.out.print( "Anna pelaajan taidot (0-99 x 10) tai 'loppu': " );
        //String ntaito = nappis.nextLine();
        //if ("loppu".equals(ntaito)) {
        //    loppu = 0;
        //}
        String ntaito = "  ";
    
        System.out.print( "Anna pelaajan numero: " );
        String nnumero= nappis.nextLine();
        if ("loppu".equals(nnumero)) {
            loppu = 0;
        }
    
        System.out.print( "Anna pelaajan rooli: " );
        String nrooli = nappis.nextLine();   
        if ("loppu".equals(nrooli)) {
            loppu = 0;
        }
        
        if ("0".equals(pallo)) {
            System.out.print( "Anna pallo pelaajalle (0 tai 1): " );
            npallo = nappis.nextLine(); 
            if ("1".equals(npallo)) {
                pallo = nnumero;
                            }
            if ("loppu".equals(npallo)) {
                loppu = 0;
                }
        
        }
        if (loppu != 0) {
            
            // luodaan uusi pelaaja olio.
            // pelaaja olion tiedot taulukoihin talteen. 
            // Ei taida olla ihan järkevää tehdä näin ????
           
            if ("maalivahti".equals(nrooli)) {
               Pelaaja maalivahti = new Maalivahti(ntaito,nnumero,nrooli,npallo);
               taitotaulu.add(maalivahti.getTaito());
               roolitaulu.add(maalivahti.getRooli());
               numerotaulu.add(maalivahti.getNumero());
               pallotaulu.add(maalivahti.getPallo());              
            
            }
            if ("hyokkaaja".equals(nrooli)) {
               Pelaaja hyokkaaja = new Hyokkaaja(ntaito,nnumero,nrooli,npallo);
               taitotaulu.add(hyokkaaja.getTaito());
               roolitaulu.add(hyokkaaja.getRooli());
               numerotaulu.add(hyokkaaja.getNumero());
               pallotaulu.add(hyokkaaja.getPallo());              
            
            }
             if ("puolustaja".equals(nrooli)) {
               Pelaaja puolustaja = new Puolustaja(ntaito,nnumero,nrooli,npallo);
               taitotaulu.add(puolustaja.getTaito());
               roolitaulu.add(puolustaja.getRooli());
               numerotaulu.add(puolustaja.getNumero());
               pallotaulu.add(puolustaja.getPallo());
             }
             if ("keskikentta".equals(nrooli)) {
               Pelaaja keskikentta = new Keskikentta(ntaito,nnumero,nrooli,npallo);
               taitotaulu.add(keskikentta.getTaito());
               roolitaulu.add(keskikentta.getRooli());
               numerotaulu.add(keskikentta.getNumero());
               pallotaulu.add(keskikentta.getPallo());
             }
             if ("pelaaja".equals(nrooli)) {
                //taulu[1] = new Pelaaja(ntaito,nnumero,nrooli,npallo);
                Pelaaja pelaaja = new Pelaaja(ntaito,nnumero,nrooli,npallo);
                taitotaulu.add(pelaaja.getTaito());
                roolitaulu.add(pelaaja.getRooli());
                numerotaulu.add(pelaaja.getNumero());
                pallotaulu.add(pelaaja.getPallo());
             }
             npallo = "0";
            }
    }
    * 
    */
    
      
       
    
/**   
// Syötellään palloa pelaajalta toiselle pallotaulussa.  
// pallotaulu = 1, kun pallo on pelaajalla.
    loppu = 1;
    syottaja = null;
    syotettava = null;
    while ( 0 < loppu ) {
        boolean syottook = false;
        System.out.print( "Anna syöttäjan numero tai 'loppu' :");
        syottaja = nappis.nextLine();
        if ("loppu".equals(syottaja)) {
           loppu = 0;
           break;
           }
        System.out.print( "Anna syotettävän numero tai 'loppu' :");
        syotettava = nappis.nextLine();
        if ("loppu".equals(syotettava)) {
            loppu = 0;
            break;
            }
                   
        int lkm = numerotaulu.size();
        for (int ix = 0; ix < lkm; ix++) {
            if (numerotaulu.get(ix).equals(syottaja) && !"0".equals(pallotaulu.get(ix))){
                syottook = true;
                pallotaulu.set(ix,"0");
                ix = lkm; }
            }
        if (syottook)  {
            syottook = false;
            for (int ix = 0; ix < lkm; ix++) {
            if (numerotaulu.get(ix).equals(syotettava)){
                syottook = true;
                pallotaulu.set(ix,"1");
                pallo = numerotaulu.get(ix);
                ix = lkm; }
                             
            }
           } 
    
        if (syottook) {
           System.out.println("Hyvä syöttö :"+syottaja+" ->"+syotettava); 
        }
        else {
           System.out.println("Syöttö ei onnistunut :"+syottaja+" ->"+syotettava);  
        }
        
    }   
    System.out.println("Pallo hallussa = 1  "   +pallotaulu);
    System.out.println("Pallo on pelaajalla "   +pallo);
    nappis.close();  
    }
   */
}
}
    
    //private Object pelimies;
