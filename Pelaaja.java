/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javapotkupallo;

import java.util.Random;

/**
 *
 * @author jarmo
 */
public class Pelaaja {
    
    public String taidot;
    String numero;
    String rooli;
    public String pallo; 
    Random random;
    
    // konstruktori.
    public Pelaaja(String ktaidot, String knumero, String krooli, String kpallo) {
         taidot = ktaidot;
         numero = knumero;
         rooli = krooli;
         pallo = kpallo;
        
      }
       
     
    // aksessorimetodit.
    public String getTaidot()               { return "5"; }
    public void setTaidot(String uusitaidot)  { taidot = uusitaidot;    }
      
    public String getNumero()             { return numero;      }
    public void setNumero(String uusinro) { numero = uusinro;   }
    
    public String getRooli()              { return rooli;       }
    public void setRooli(String uusirooli){ rooli = uusirooli;  }
        
    
    public void setPallo(String uusipallo){ pallo = uusipallo;  }
    public String getPallo()              { return pallo;}  
    
    public String syotaPallo(String pallo){
        pallo = "0";
        return pallo;
        }
    public String nappaaPallo(String kpallo){
        kpallo = "1";
        return kpallo;
        }
    
    // tuuriPallo palauttaa OK tai NotOK onnistuuko pelaajan operaatio taidon todennäköisyyden mukaan.
    public String tuuriPallo(String pallo){
        int tuuri = Integer.parseInt(this.getTaidot());
        float tuurif = tuuri;
        tuurif = tuurif / 10;
        this.random = new Random();
        double todennakoisyys = this.random.nextDouble();
        if (todennakoisyys >= tuurif) {
            pallo = "1"; 
            System.out.println("* "+getRooli()+" onnistuu. ");
            return pallo;
            }
               
        pallo = "0"; 
        this.setPallo(pallo);
        System.out.println("* "+getRooli()+" epäonnistuu. ");
        if ("hyokkaaja".equals(rooli)) {
            pallo = "maali";
        }
        return pallo;
    }      

   }

